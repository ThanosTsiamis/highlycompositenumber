This project is a direct consequence of the exercise that Instructor Greg gave us on the Agile Actors Academy September 2019.
Below we provide in short the description of the exercise:
Write a program that uses multiple threads to find the integer in a range that has the largest number of divisors.
The range is from 1 to 100000.
By using threads, your program will take less time to do the computation when it is run on a multiprocessor computer. At the end of the program, output the elapsed time, the integer that has the largest number of divisors, and the number of divisors that it has.

You may be wondering where the name Highly Compoiste Number of the project came from.
Well, the exercise is exactly that.However,as of today(14/11/2019)I'm not sure how can I multiprocess it,given that one follows my train of thought.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Explanation~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Firstly notice that each number less than or equal to 100,000 has at most 6 different prime divisors since 2x3x5x7x11x13x15>100,000 .
If n=p^e1⋯p^er, then the number of divisors d(n) of n is given by d(n)=(e1+1)(e2+1)…(er+1).
Using some more maths (proof of that will be given later on) we find out that:
0<=e1<=16
0<=e2<=10
0<=e3<=7
0<=e4<=5
0<=e5<=4
0<=e6<=4

and also:
Πpi^ej<=100,000

and also:
e1>=e2>=e3>=e4>=e5>=e6>=0


As a direct results of the limitations the problem imposed, we make a nested loop and we break each loop if restriction No.3 is not fulfilled.
Using battery saving mode the computer takes on average 15ms.

